

var gulp                                                      = require('gulp');
const template                                       = require('gulp-template');
var concat                                             = require('gulp-concat');
const                                                  fs = require('fs-extra');
var util                                                = require('gulp-util');




module.exports = {
                    make_template: function ()
                    {



                        var DNAME = (util.env.name).toUpperCase();
                        var NAME = util.env.name;
                        var options = {

                            SASS_IN           :  "000_TEMPLATE/01_SASS" ,
                            SASS_OUT          :  "01_SASS/"+DNAME ,
                            SCRIPTS_IN        :  "000_TEMPLATE/02_SCRIPTS" ,
                            SCRIPTS_OUT       :  "02_SCRIPTS/"+DNAME ,
                            WORKSPACE_IN      :  "000_TEMPLATE/04_WORK_SPACE" ,
                            WORKSPACE_OUT     :  "03_ViewsT/aa_WorkSpace/"+DNAME,
                            FRAGMENTS_IN      :  "000_TEMPLATE/05_FRAGMENTS" ,
                            FRAGMENTS_OUT     :  "03_ViewsT/ab_Fragments/"+DNAME,
                            GULP_IN           :  "000_TEMPLATE/06_GULP_PAGE" ,
                            GULP_OUT          :  "zz_tasks/aa_WEBPAGES",
                            IMAGES_IN         :  "000_TEMPLATE/03_IMAGES",
                            IMAGES_OUT        :  "04_IMAGES/"+DNAME,
                            'NAME'            :  util.env.name,
                            'DNAME'           :  DNAME,
                            'DBASE'           :  util.env.dbase
                        };

                        var result_scripts=gulp.src(options.SCRIPTS_IN+"/**/*.*")
                            .pipe(gulp.dest(options.SCRIPTS_OUT));


                        var result_sass=gulp.src(options.SASS_IN+"/**/*.*")
                            .pipe(gulp.dest(options.SASS_OUT));






/// Views

                        var result_workspace=gulp.src(options.WORKSPACE_IN+"/**/*.*")
                            .pipe(template({NAME:options.NAME,DNAME:options.DNAME}))
                            .pipe(gulp.dest(options.WORKSPACE_OUT));


                        var result_fragment=gulp.src(options.FRAGMENTS_IN+"/**/*.*")
                            .pipe(gulp.dest(options.FRAGMENTS_OUT));

                        var result_images=gulp.src(options.IMAGES_IN+"/**/*.*")
                            .pipe(gulp.dest(options.IMAGES_OUT));

//gulp
                        var result_gulp=gulp.src(options.GULP_IN+"/**/*.*")
                            .pipe(template({NAME:options.NAME,DNAME:options.DNAME}))
                            .pipe(concat(options.NAME+".js"))
                            .pipe(gulp.dest(options.GULP_OUT).on('end',function () {

                                make_config();

                            }));



                        var execSync = require('child_process').execSync;
                        execSync('php artisan make:controller '+options.DNAME+'/'+"main",{
                            cwd:'aa_template'
                        });
                        console.log("done controller");

                        var execSync = require('child_process').execSync;

                        var dest = "app/Http/Controllers/"+options.DNAME+'/'+"main.php";
                        var route ='sed -i '+'/'+"**"+'/d '+'./'+dest;
                        execSync(route,{
                            cwd:'aa_template'
                        });

                        var data ="<?php\n" +
                            "\n" +
                            "namespace App\\Http\\Controllers\\"+options.DNAME+";\n" +
                            "\n" +
                            "use Illuminate\\Http\\Request;\n" +
                            "use App\\Http\\Controllers\\Controller;\n" +
                            "\n" +
                            "class main extends Controller\n" +
                            "{\n" +
                            "    //\n" +
                            "public  function "+"_"+options.NAME+"(){\n" +
                            "\n" +
                            "    return view(\""+options.DNAME+".main\");\n" +
                            "}\n" +
                            "\n" +
                            "\n" +
                            "}\n";

                        var prependFile = require('prepend-file');
                        prependFile.sync("aa_template/"+dest, data);

                        //
                       // sed -i 's/\<is\>/& not/' /tmp/filename

                        // console.log("A");
                        if(options.DBASE !== undefined)
                        {
                            //  console.log("B");
                            make_dbase_template(options);
                        }



                        const dest_root = "aa_template/routes/web.php";
                        const dummy = "Route::get(";
                        const dest_part_1='Route::get(\"/'+options.NAME+'\",'+"\""+options.DNAME+'\\'+"main"+'@\_'+options.NAME+'\");\n';
                        fs.appendFileSync(dest_root, dest_part_1);
                        console.log("done route  Injection _____________::"+dest_part_1);





                        return result_sass && result_scripts &&  result_workspace
                            && result_fragment && result_gulp && result_images;




                    },

                    remove_template: function ()
                    {

                        var                                     result,i;
                        var execSync = require('child_process').execSync;
                        var del                          = require('del');
                        const fs = require('fs-extra');

                        var DNAME = (util.env.name).toUpperCase();
                        var NAME =  util.env.name;





                        var dir = [
                            ['SASS_CLEAN','01_SASS/'+DNAME ],
                            ['SCRIPTS_CLEAN','02_SCRIPTS/'+DNAME ],
                            ['VIEW_CLEAN_W','03_ViewsT/aa_WorkSpace/'+DNAME],
                            ['VIEW_CLEAN_F','03_ViewsT/ab_Fragments/'+DNAME],
                            ['VIEW_CLEAN_B','03_ViewsT/aa_WorkSpace/zz_body/'+DNAME],
                            ['CONTROLLER_CLEAN','aa_template/app/Http/Controllers/'+DNAME],
                            ['MODEL_CLEAN','aa_template/app/Models/'+DNAME],
                            ['IMAGE_CLEAN','04_IMAGES/'+DNAME],
                            ['RESOURCE_VIEW','aa_template/resources/views/'+DNAME]






                        ];

                        var files = [





                            ['SEED_CLEAN','aa_template/database/seeds/*'+NAME+"*.php"],
                            ['MIGRATION_CLEAN','aa_template/database/migrations/*'+NAME+"*.php"],
                            ['GULP_CLEAN',"zz_tasks/aa_WEBPAGES/"+NAME+".js"],
                            ['CSS_CLEAN',"aa_template/public/01_CSS"+NAME+".js"],
                            ['JS_CLEAN',"aa_template/public/02_SCRIPTS"+NAME+".js"]




                        ];



                        //console.log(dir[2][1]);
                        //console.log(dir.length);

                        for (i = 0; i < dir.length; i++) {
                            console.log(dir[i][1]);


                            fs.removeSync(dir[i][1]);
                            /*

                            result=fs.remove(dir[i][1], err => {
                                if (err) return console.error(err)


                            });*/
                        }



                        for (i = 0; i < files.length; i++) {
                            console.log("*"+files[i][1]);
                            del.sync(files[i][1]);

                            console.log("done Factory"+files[i][1]);
                        }

                        var route ='sed -i '+'/'+NAME+'/d '+'./routes/web.php';


                        execSync(route,{
                            cwd:'aa_template'
                        });

                        make_config();




                        return true;


                    },

                    make_db: function ()
                    {
                        return make_dbase_template();
                    },
                    laravel_cache_dump_autoload: function ()
                    {


                        var execSync = require('child_process').execSync;

                        execSync('composer dump-autoload',{
                            cwd:'aa_template'
                        });


                        var execSync1 = require('child_process').execSync;

                        execSync1('php artisan config:cache',{
                            cwd:'aa_template'
                        });


                        var execSync2 = require('child_process').execSync;

                        execSync2('php artisan cache:clear',{
                            cwd:'aa_template'
                        });


                        /*
                      execSync('php artisan config:cache',{
                          cwd:'aa_template'
                      });
                      execSync('php artisan cache:clear',{
                          cwd:'aa_template'
                      });
                      */


                        console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                        return execSync1;
                    },
                    delete_db: function ()
                    {
                        var                                     result,i;
                        var execSync = require('child_process').execSync;
                        var del                          = require('del');


                        var DNAME = (util.env.name).toUpperCase();
                        var NAME =  util.env.name;

                        var options = {

                            SASS_IN           :  "000_TEMPLATE/01_SASS" ,
                            SASS_OUT          :  "01_SASS/"+DNAME ,
                            SCRIPTS_IN        :  "000_TEMPLATE/02_SCRIPTS" ,
                            SCRIPTS_OUT       :  "02_SCRIPTS/"+DNAME ,
                            WORKSPACE_IN      :  "000_TEMPLATE/04_WORK_SPACE" ,
                            WORKSPACE_OUT     :  "03_ViewsT/aa_WorkSpace/"+DNAME,
                            FRAGMENTS_IN      :  "000_TEMPLATE/05_FRAGMENTS" ,
                            FRAGMENTS_OUT     :  "03_ViewsT/ac_Fragments/"+DNAME,
                            GULP_IN           :  "000_TEMPLATE/06_GULP_PAGE" ,
                            GULP_OUT          :  "zz_tasks/aa_WEBPAGES",
                            BODY_IN           :  "03_ViewsT/ab_Body/"+DNAME ,
                            'NAME'            :  util.env.name,
                            'DNAME'           :  DNAME,
                            'DBASE'           :  util.env.dbase
                        };
                        var dbase_C = options.DBASE.charAt(0).toUpperCase()+ options.DBASE.slice(1);
                        var dbase_model =dbase_C.substring(0, dbase_C.length - 1);



                        var files = [





                            ['SEED_CLEAN','aa_template/database/seeds/*'+options.NAME+"__"+options.DBASE+"*.php"],
                            ['MIGRATION_CLEAN','aa_template/database/migrations/*'+NAME+"__"+options.DBASE+"*.php"],
                            ['MODEL_CLEAN','aa_template/app/Models/'+options.DNAME+"/"+dbase_model+"*.php"]




                        ];

                        for (i = 0; i < files.length; i++) {
                            console.log("*"+files[i][1]);
                            del.sync(files[i][1]);

                            console.log("done Factory"+files[i][1]);
                        }



                    }





};


function make_dbase_template()
{

    var DNAME = (util.env.name).toUpperCase();
    var NAME = util.env.name;

    var options = {

        SASS_IN           :  "000_TEMPLATE/01_SASS" ,
        SASS_OUT          :  "01_SASS/"+DNAME ,
        SCRIPTS_IN        :  "000_TEMPLATE/02_SCRIPTS" ,
        SCRIPTS_OUT       :  "02_SCRIPTS/"+DNAME ,
        WORKSPACE_IN      :  "000_TEMPLATE/04_WORK_SPACE" ,
        WORKSPACE_OUT     :  "03_ViewsT/aa_WorkSpace/"+DNAME,
        FRAGMENTS_IN      :  "000_TEMPLATE/05_FRAGMENTS" ,
        FRAGMENTS_OUT     :  "03_ViewsT/ac_Fragments/"+DNAME,
        GULP_IN           :  "000_TEMPLATE/06_GULP_PAGE" ,
        GULP_OUT          :  "zz_tasks/aa_WEBPAGES",
        BODY_IN           :  "03_ViewsT/ab_Body/"+DNAME ,
        'NAME'            :  util.env.name,
        'DNAME'           :  DNAME,
        'DBASE'           :  util.env.dbase
    };
    var dbase_C = options.DBASE.charAt(0).toUpperCase()+ options.DBASE.slice(1);
    var dbase_model =dbase_C.substring(0, dbase_C.length - 1);


    var execSync = require('child_process').execSync;
    execSync('php artisan make:migration '+options.NAME+"__"+options.DBASE+" --create="+options.DBASE,{
        cwd:'aa_template'
    });
    console.log("done migration"+options.NAME+"########################");






    execSync('php artisan make:model '+"/Models/"+options.DNAME+"/"+dbase_model,{
        cwd:'aa_template'
    });
    console.log("done Model");


    execSync('php artisan make:seeder '+options.NAME+"__"+options.DBASE,{
        cwd:'aa_template'
    });
    console.log("done Seeding");

    var execSync = require('child_process').execSync;
    var dest = "database/seeds/"+options.NAME+"__"+options.DBASE+".php";

    var route ='sed -i '+'/'+"**"+'/d '+'./'+dest;
    execSync(route,{
    cwd:'aa_template'
});



    var model_name = "App\\Models\\"+options.DNAME+"\\"+dbase_model;
    var data ="<?php\n" +
        "\n" +
        "use Illuminate\\Database\\Seeder;\n" +
        "\n" +
        "class "+options.NAME+"_"+options.DBASE+" extends Seeder\n" +
        "{\n" +
        " \n" +
        "    /**\n" +
        "     * Run the database seeds.\n" +
        "     *\n" +
        "     * @return void\n" +
        "     */\n" +
        "    public function run()\n" +
        "    {\n" +
        "\t$factory=factory("+model_name +"::class,10);\n\t $factory->create();\n" +
        "        //\n" +
        "    }\n" +
        "}";

    var prependFile = require('prepend-file');
    prependFile.sync("aa_template/"+dest, data);



    console.log("done Seed Injection");






    execSync('php artisan test-factory-helper:generate',{
        cwd:'aa_template'
    });
    console.log("done Factory");

    return execSync;

}

function make_config( )
{
    //console.log(configf.items);

    var              execSync = require('child_process').execSync;
    var                     prependFile = require('prepend-file');
    const                                      fs = require('fs');


    const                  testFolder = './zz_tasks/aa_WEBPAGES/';
    var                                    file_name= 'config.js';
    var                                        dest = "config.js";


    //Make file
    execSync('touch config.js');
    //clear file if exists
    var route ='sed -i '+'/'+"**"+'/d '+'./'+dest;
    execSync(route);

    var page_link ="\n module.exports.pagelink=\"http://127.0.0.1/"+util.env.name+"\";";
    var start ="module.exports.items = [\n";
    var middle ="";
    var end ="\n];";
    var file_sum="";

    var initV="module.exports.VALID =1;\n" +
        "module.exports.INVALID =0;\n";



    var flag_first=true;
    fs.readdirSync(testFolder).forEach(file => {

        var trimmed_file =   file.substring(0, file.length - 3);
        if(flag_first)
        {
            middle=( '\t['+'\"'+trimmed_file+'\"'+',module.exports.VALID]');
            flag_first=false;
        }
        else
            middle=( ',\n\t['+'\"'+trimmed_file+'\"'+',module.exports.VALID]');

        file_sum=file_sum+middle;

        console.log(file);


    });

    prependFile.sync("config.js", initV+start+file_sum+end+page_link);


    var xx = require('../../config.js');

    console.log(xx.items);

    console.log("Done");

    return true;


}
