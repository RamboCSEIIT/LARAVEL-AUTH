var gulp = require('gulp');
// Image compression
var imagemin = require('gulp-imagemin');
var imageminJpegRecompress = require('imagemin-jpeg-recompress');
var IMAGES_IN_PATH = '04_IMAGES/**/*.{png,jpeg,jpg,svg,gif}';
var IMAGES_OUT_PATH = 'aa_template/public/03_IMAGES';








module.exports =
        {

            image_move: function ()
            {
                var result = gulp.src(IMAGES_IN_PATH)
                        .pipe(imagemin([
                            imagemin.gifsicle(),
                            imageminJpegRecompress({
                                loops: 4,
                                min: 30,
                                max: 50,
                                quality: 'high'
                            }),
                            imagemin.optipng(),
                            imagemin.svgo()
                        ])).pipe(gulp.dest(IMAGES_OUT_PATH));


                return result;


            }
        



        };