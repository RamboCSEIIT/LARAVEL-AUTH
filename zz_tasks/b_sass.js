        
        
var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpif = require('gulp-if');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');

var cssbeautify = require('gulp-cssbeautify');
var compass = require('compass-importer');
var concat = require('gulp-concat');
var sourcemaps                                     = require('gulp-sourcemaps');

  
module.exports =
        {

            sass_move: function ()
            {
                return gulp.src( process.env.SASS_IN_PATH )
                
                
                      
                        .pipe(gulpif(process.env.NODE_ENV === 'development',
                                sass({

                                    importer: compass

                                }).on('error', function (err)
                        {
                            notify().write(err);
                            this.emit('end');
                        }),
                                sass(
                                        {
                                            outputStyle: 'compressed',
                                            importer: compass

                                        }
                                ).on('error', function (err)
                        {
                            notify().write(err);
                            this.emit('end');
                        })))
                        .pipe(gulpif(process.env.NODE_ENV === 'development', cssbeautify({
                            indent: '  ',
                            openbrace: 'separate-line',
                            autosemicolon: true
                        })))
                         .pipe(autoprefixer())       
                        .pipe(concat(process.env.SASS_OUT_FILE_NAME))
                        .pipe(gulp.dest(process.env.SASS_OUT_PATH));

            }
            
            
  


        };
        
        
global.SCSS = function (options)
{

       return gulp.src( options.SASS_IN_PATH )
                
                
                      
                        .pipe(gulpif(process.env.NODE_ENV === 'development',
                                sass({

                                    importer: compass

                                }).on('error', function (err)
                        {
                            notify().write(err);
                            this.emit('end');
                        }),
                                sass(
                                        {
                                            importer: compass,
                                            outputStyle: 'compressed',
                                        }
                                ).on('error', function (err)
                        {
                            notify().write(err);
                            this.emit('end');
                        })))
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.write()))
                        
                        .pipe(gulpif(process.env.NODE_ENV === 'development', cssbeautify({
                            indent: '  ',
                            openbrace: 'separate-line',
                            autosemicolon: true
                        })))
                         .pipe(autoprefixer({
                             browsers: ['last 5 versions'],
                             cascade: false
                         }))
                        .pipe(concat(options.SASS_OUT_FILE_NAME))
                        .pipe(gulp.dest(options.SASS_OUT_PATH));

};