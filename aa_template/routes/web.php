<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();
Route::get('/home', 'HomeController@settings')->name('home');

//###################################################################################################### //
Route::get('admin/home', 'AA_ADMIN_AA_HOME\index@dashboard')->name('admin.home');//#
#Route::get('testC', 'AdminController@settings')->name('admin.home');//#


Route::get('admin/editor', 'EditorController@settings')->name('edit.home');//#
Route::get('admin/test','EditorController@test');

Route::get('admin' , 'Admin\LoginController@showLoginForm')->name('admin.login');//#
Route::post('admin', 'Admin\LoginController@login');

Route::get('admin-password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');


Route::get('admin-password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
Route::post('admin-password/reset', 'Admin\ResetPasswordController@reset')->name('admin.password.reset.request');


Route::get('verifyEmailFirst', 'Auth\RegisterController@verifyEmailFirst')->name('verifyEmailFirst');


Route::get('verify/{email}/{verifyToken}', 'Auth\RegisterController@sendEmailDone')->name('sendEmailDone');

 
Route::get("admin/settings","AA_ADMIN_AB_SETTING\settings@admin_settings");

Route::get("/","AC_WELCOME\main@_ac_welcome");
Route::get("/zz_compass","ZZ_COMPASS\main@_zz_compass");
