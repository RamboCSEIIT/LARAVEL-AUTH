#TestCase
{
  width: 100%;
  height: 500px;
  color: red;
}

#TestCase .intro
{
  width: 100%;
  height: 500px;
  color: yellow;
  border-radius: 20px;
  background-image: -owg-linear-gradient(white, red);
  background-image: -webkit-linear-gradient(white, red);
  background-image: -o-linear-gradient(white, red);
  background-image: -webkit-gradient(linear, left top, left bottom, from(white), to(red));
  background-image: linear-gradient(white, red);
}
