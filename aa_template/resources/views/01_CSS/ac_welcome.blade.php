@import url("https://fonts.googleapis.com/css?family=Roboto:700");

#navabar-top .pale-pink
{
  background-color: #ffb6c1;
}

#navabar-top a
{
  color: white;
  text-decoration: none;
  margin-left: 40px;
  font-family: 'Roboto';
  border-bottom: 1px solid rgba(254, 254, 254, 0);
  -webkit-transition: border-bottom .5s;
  -o-transition: border-bottom .5s;
  transition: border-bottom .5s;
  -webkit-transition-timing-function: ease-in-out;
  -o-transition-timing-function: ease-in-out;
  transition-timing-function: ease-in-out;
}

#navabar-top a:hover
{
  border-bottom: 1px solid #fefefe;
  color: blue;
}

#sidebar-top a
{
  font-family: 'Roboto';
}

#sidebar-top .pale-pink
{
  background-color: #ffb6c1;
}

#sidebar-top .sidebar
{
  left: -250px;
  width: 250px;
  height: 100%;
  -webkit-transition: .5s ease-in;
  -o-transition: .5s ease-in;
  transition: .5s ease-in;
}

#sidebar-top .active
{
  left: 0;
}

#sidebar-top ul
{
  margin: 0;
  padding: 0px 0;
}

#sidebar-top li
{
  list-style: none;
  text-align: center;
}

#sidebar-top a
{
  color: #fff;
  padding: 10px 20px;
  display: block;
  text-decoration: none;
  border-bottom: 1px solid rgba(0, 0, 0, 0.2);
}

#sidebar-top .SideBarButton
{
  position: absolute;
  top: 0;
  right: -50px;
  width: 50px;
  height: 50px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  cursor: pointer;
  border: none;
  outline: none;
}

#sidebar-top .SideBarButton span
{
  display: block;
  width: 35px;
  height: 3px;
  position: absolute;
  top: 24px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-top .SideBarButton span:before
{
  content: '';
  position: absolute;
  top: -10px;
  left: 0;
  width: 100%;
  height: 3px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-top .SideBarButton span:after
{
  content: '';
  position: absolute;
  top: 10px;
  left: 0;
  width: 100%;
  height: 3px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-top .SideBarButton.toggle span
{
  background: transparent;
}

#sidebar-top .SideBarButton.toggle span:before
{
  top: 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

#sidebar-top .SideBarButton.toggle span:after
{
  top: 0;
  -webkit-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  transform: rotate(-45deg);
}

#sidebar-nav-menu .pale-pink
{
  background-color: #ffb6c1;
}

#sidebar-nav-menu .SideBarButton
{
  position: absolute;
  width: 50px;
  height: 50px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  cursor: pointer;
  border: none;
  outline: none;
}

#sidebar-nav-menu .SideBarButton span
{
  display: block;
  width: 35px;
  height: 3px;
  position: absolute;
  top: 24px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-nav-menu .SideBarButton span:before
{
  content: '';
  position: absolute;
  top: -10px;
  left: 0;
  width: 100%;
  height: 3px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-nav-menu .SideBarButton span:after
{
  content: '';
  position: absolute;
  top: 10px;
  left: 0;
  width: 100%;
  height: 3px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-nav-menu .SideBarButton.toggle span
{
  background: transparent;
}

#sidebar-nav-menu .SideBarButton.toggle span:before
{
  top: 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

#sidebar-nav-menu .SideBarButton.toggle span:after
{
  top: 0;
  -webkit-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  transform: rotate(-45deg);
}

#sidebar-nav-menu .navbar-brand
{
  position: absolute;
  left: 50%;
  -webkit-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  transform: translateX(-50%);
}

#sidebar-nav-menu .hamberger-menu
{
  position: absolute;
  -webkit-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  transform: translateX(-50%);
  margin-left: 3%;
}

#sidebar-nav-menu .logo-background
{
  background-color: #ffb6c1;
}

#sidebar-nav-menu .circle-bg
{
  margin: 20px;
}
