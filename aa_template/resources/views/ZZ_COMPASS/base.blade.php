<!DOCTYPE html>
<html lang=en>

<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta http-equiv=ScreenOrientation content=autoRotate:disabled>
    <meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name=description content="">
    <meta name=author content="">
    <link rel="shortcut icon" type=image/png href=02_IMAGES/favicon.png>
    <link rel=stylesheet href=https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css type=text/css>
    <link rel=stylesheet href=https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css type=text/css>
    <title>@yield('title')</title>
    <style>  @include('01_CSS.zz_compass') </style>
</head>

<body>
@yield('content')
<script src=https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js></script>
<script src=https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js></script>
<script src=https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js></script>
<script type="text/javascript"> @include('00_SCRIPTS.zz_compass')</script>
</body>

</html>